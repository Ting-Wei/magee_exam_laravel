<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class record_new extends Model
{
    public $timestamps = false;
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'record';
    protected $guarded = [];
}
