<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class member_new extends Model
{
    public $timestamps = false;
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'member';
    protected $guarded = [];
}
