<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class question_new extends Model
{
    public $timestamps = false;
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'questions';
    protected $guarded = [];
}
