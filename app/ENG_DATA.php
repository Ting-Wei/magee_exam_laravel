<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ENG_DATA extends Model
{
    protected $connection = 'mysql2';
    protected $primaryKey = 'id';
    protected $table = 'ENG_DATA';
}
