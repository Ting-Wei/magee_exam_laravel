<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    protected $connection = 'mysql2';
    public $timestamps = false;
    protected $primaryKey = 'pk';
    protected $table = 'record';
    protected $fillable = [
        'mem_id', 'exam_re'
    ];
}
