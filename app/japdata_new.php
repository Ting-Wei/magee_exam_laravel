<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class japdata_new extends Model
{
    public $timestamps = false;
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'jap_data';
    protected $guarded = [];
}
