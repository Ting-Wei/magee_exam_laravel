<?php

namespace App\Http\Controllers;

use App\QUESTIONS;
use App\question_new;
use App\engdata_new;
use App\japdata_new;
use App\JAP_DATA;
use App\ENG_DATA;
use App\Member;
use App\member_new;
use App\Record;
use App\record_new;
use App\SwitchTables;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
// use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function testDB()
    {
        ini_set('max_execution_time', 30000);
        $data = DB::connection('mysql2')->table('eng_member')->where('data_sts', '=', 'Y')->get();
        // $data =  JAP_DATA::where('data_sts', '=', 'Y')->orderBy('pk')->get();
        //return $data;
        // $data2 =  JAP_DATA::get();
        // $data3 =  ENG_DATA::get();

        // $data->map(function ($item, $index) {
        //     // dd($item->isclast_cname);
        //     $new_que = new question_new;
        //     $new_que->year = $item->year;
        //     $new_que->questions_cname = $item->questions_cname;
        //     $new_que->qa_kind_id = $item->qa_kind_id;
        //     $new_que->qa_ans_id = $item->qa_ans_id;
        //     $new_que->qa_lan_id = $item->qa_lan_id;
        //     $new_que->course_id = $item->course_id;
        //     $new_que->ansA = $item->ans1;
        //     $new_que->ansB = $item->ans2;
        //     $new_que->ansC = $item->ans3;
        //     $new_que->ansD = $item->ans4;
        //     $new_que->notes = $item->notes;
        //     $new_que->notes1 = $item->notes1;
        //     $new_que->notes2 = $item->notes2;
        //     $new_que->notes3 = $item->notes3;
        //     $new_que->notes4 = $item->notes4;
        //     $new_que->correct_ans1 = $item->ans1_id;
        //     $new_que->correct_ans2 = $item->ans2_id;
        //     $new_que->correct_ans3 = $item->ans3_id;
        //     $new_que->correct_ans4 = $item->ans4_id;
        //     $new_que->idept_id = $item->idept_id;
        //     $new_que->isdept_id = $item->isdept_id;
        //     $new_que->iclas_id = $item->iclas_id;
        //     $new_que->isclas_id = $item->isclas_id;
        //     $new_que->idept_cname = $item->idept_cname;
        //     $new_que->isdept_cname = $item->isdept_cname;
        //     $new_que->iclas_cname = $item->iclas_cname;
        //     $new_que->isclas_cname = $item->isclas_cname;
        //     $new_que->editor_type_id = $item->editor_type_id;
        //     $new_que->exam_id = $item->exam_id;
        //     $new_que->save();
        // });

        // $data->map(function ($item, $index) {
        //     // dd($item->isclast_cname);
        //     $new_que = new japdata_new;
        //     $new_que->year = $item->year;
        //     $new_que->questions_cname = $item->questions_cname;
        //     $new_que->qa_kind_id = $item->qa_kind_id;
        //     $new_que->qa_ans_id = $item->qa_ans_id;
        //     $new_que->grp = $item->grp;
        //     $new_que->art = $item->art;
        //     $new_que->ansA = $item->ans1;
        //     $new_que->ansB = $item->ans2;
        //     $new_que->ansC = $item->ans3;
        //     $new_que->ansD = $item->ans4;
        //     $new_que->notes = $item->notes;
        //     $new_que->notes1 = $item->notes1;
        //     $new_que->notes2 = $item->notes2;
        //     $new_que->notes3 = $item->notes3;
        //     $new_que->notes4 = $item->notes4;
        //     $new_que->art_notes = $item->art_notes;
        //     $new_que->pic = $item->pic;
        //     $new_que->art_tip = $item->art_tip;
        //     $new_que->correct_ans1 = $item->ans1_id;
        //     $new_que->correct_ans2 = $item->ans2_id;
        //     $new_que->correct_ans3 = $item->ans3_id;
        //     $new_que->correct_ans4 = $item->ans4_id;
        //     $new_que->idept_id = $item->idept_id;
        //     $new_que->isdept_id = $item->isdept_id;
        //     $new_que->iclas_id = $item->iclas_id;
        //     $new_que->isclas_id = $item->isclas_id;
        //     $new_que->editor_type_id = $item->editor_type_id;
        //     $new_que->exam_id = $item->exam_id;
        //     $new_que->save();
        // });

        // $data->map(function ($item, $index) {
        //     // dd($item->isclast_cname);
        //     $new_que = new member_new();
        //     $new_que->member_id = $item->member_id;
        //     $new_que->password = $item->password;
        //     $new_que->member_cname = $item->member_cname;
        //     $new_que->id_no = $item->id_no;
        //     $new_que->gender_id = $item->gender_id;
        //     $new_que->birth_date = $item->birth_date;
        //     $new_que->post_id = $item->post_id;
        //     $new_que->address = $item->address;
        //     $new_que->member_type_id = $item->member_type_id;
        //     $new_que->phone_no = $item->phone_no;
        //     $new_que->mobile_no = $item->mobil_no;
        //     $new_que->email = $item->email;
        //     $new_que->epaper = $item->epaper;
        //     $new_que->img = $item->img;
        //     $new_que->menver_img = $item->menver_img;
        //     $new_que->point = $item->point;
        //     $new_que->member_level = $item->member_level;
        //     $new_que->member_jobtype = $item->member_jobtype;
        //     $new_que->member_edu = $item->member_edu;
        //     $new_que->member_worksatae = $item->member_worksatae;
        //     $new_que->member_department = $item->member_department;
        //     $new_que->member_license = $item->member_license;
        //     $new_que->member_skill = $item->member_skill;
        //     $new_que->pipeline = $item->pipeline;
        //     $new_que->api_token = $item->api_token;
        //     $new_que->save();
        // });

        // $data->map(function ($item, $index) {
        //     // dd($item->isclast_cname);
        //     $new_que = new record_new();
        //     $new_que->mem_id = $item->mem_id;
        //     $new_que->exam_re = $item->exam_re;
        //     $new_que->save();
        // });

        $data->map(function ($item, $index) {
            DB::connection('mysql')->table('eng_member')->insertGetId(
                [
                    'user' => $item->user,
                    'pass' => $item->pass,
                    'level' => $item->level,
                    'nick' => $item->nick,
                ],
            );
        });


        // $allDB = collect(["QUESTIONS", "JAP_DATA", "ENG_DATA"]);

        // $aaa = $allDB->map(function ($item, $index) {
        //     $bbb = ("\App\\" . $item)::get();
        //     return $bbb;
        // });

        // return $aaa;

        // return DB::table("questions")->skip(0)->take(20)->get();
        // return "hello";
    }
}
