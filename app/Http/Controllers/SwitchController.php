<?php

namespace App\Http\Controllers;

use App\QUESTIONS;
use App\JAP_DATA;
use App\ENG_DATA;
use App\question_new;
use App\japdata_new;
use App\engdata_new;
use App\member_new;
use App\Member;
use App\SwitchTables;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\Cast\Object_;
use function PHPSTORM_META\type;

class SwitchController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function getexampage($table, $year, $examId, $method = "test", $page = 1)
    {
        // $year = str_pad($year, 3, 0, STR_PAD_LEFT);
        $year = sprintf("%03d", $year);
        $select = ["C" => "question_new", "E" => "engdata_new", "J" => "japdata_new"];
        $where = ["exam_id" => "EXAM_" . $year . $examId];
        $pagenumber = 10;
        $select[$table];
        $questions = ("\App\\" . $select[$table])::where($where);
        // return $result->get();
        if ($method == "test") {
            $questions = $questions->skip(($page - 1) * $pagenumber)->take($pagenumber);
        }
        // $art = strip_tags($questions->get("art"));

        // return $art->unique('art');

        $data = $questions->get()->groupBy([
            'qa_ans_id',
            function ($item) {
                return $item['grp'];
            },
        ]);
        // $data = $questions->get()->groupBy('qa_ans_id');
        // return $data->toArray();

        $questionnum = 0;
        $TopQuestionNum = 0;
        $questionAns = collect();

        $single = $data["1"]->shift()->map(function ($value, $index) use (&$questionnum, &$questionAns) {
            $questionnum++;
            $questionAns->push(["ans" => [$value->correct_ans1], "group" => $questionnum]);
            return [
                "questionnumber" => $questionnum,
                "status" => false,
                "selectq" => "",
                "ans" => [$value->correct_ans1],
                "art" => strip_tags($value->art),
                "qa_ans_id" => $value->qa_ans_id,
                "grp" => $value->grp,
                "title" => $value->questions_cname,
                "notes_title" => $value->notes,
                "sel" => [
                    [
                        "name" => $value->ansA,
                        "notes" => $value->notes1,
                        "value" => "A"
                    ],
                    [
                        "name" => $value->ansB,
                        "notes" => $value->notes2,
                        "value" => "B"
                    ],
                    [
                        "name" => $value->ansC,
                        "notes" => $value->notes3,
                        "value" => "C"
                    ],
                    [
                        "name" => $value->ansD,
                        "notes" => $value->notes4,
                        "value" => "D"
                    ]
                ]
            ];
        });
        $plural = collect();
        if (isset($data["3"])) {
            $plural = $data["3"]->shift()->map(function ($value, $index) use (&$questionnum, &$questionAns) {
                $questionnum++;
                $questionAns->push([
                    "ans" => collect([
                        $value->correct_ans1,
                        $value->correct_ans2,
                        $value->correct_ans3,
                        $value->correct_ans4
                    ])->filter()->values(),
                    "group" => $questionnum]);
                return [
                    "questionnumber" => $questionnum,
                    "status" => false,
                    "selectq" => "",
                    "ans" => collect([
                        $value->correct_ans1,
                        $value->correct_ans2,
                        $value->correct_ans3,
                        $value->correct_ans4
                    ])->filter()->values(),
                    "art" => strip_tags($value->art),
                    "qa_ans_id" => $value->qa_ans_id,
                    "grp" => $value->grp,
                    "title" => $value->questions_cname,
                    "notes_title" => $value->notes,
                    "sel" => [
                        [
                            "name" => $value->ansA,
                            "notes" => $value->notes1,
                            "value" => "A"
                        ],
                        [
                            "name" => $value->ansB,
                            "notes" => $value->notes2,
                            "value" => "B"
                        ],
                        [
                            "name" => $value->ansC,
                            "notes" => $value->notes3,
                            "value" => "C"
                        ],
                        [
                            "name" => $value->ansD,
                            "notes" => $value->notes4,
                            "value" => "D"
                        ]
                    ]
                ];
            });
        }

        $TopQuestionNum = $questionnum;

        $fillin = collect();
        if (isset($data["4"])) {
            $fillin = $data["4"]->map(function ($item, $index) use (&$questionnum, &$questionAns, &$TopQuestionNum) {
                $TopQuestionNum++;
                return [
                    "fill" => [
                        "art" => $item[0]->art,
                        "art_notes" => $item[0]->art_notes,
                        "data" => $item->map(function ($value, $keys) use (&$questionnum, &$questionAns, &$TopQuestionNum) {
                            $questionnum++;
                            $questionAns->push([
                                "ans" => [$value->correct_ans1],
                                "group" => $TopQuestionNum
                            ]);
                            return [
                                "questionnumber" => $questionnum,
                                "status" => false,
                                "selectq" => "",
                                "ans" => [$value->correct_ans1],
                                // "art" => strip_tags($value->art),
                                "qa_ans_id" => $value->qa_ans_id,
                                "grp" => $value->grp,
                                "title" => $value->questions_cname,
                                "notes_title" => $value->notes,
                                "sel" => [
                                    [
                                        "name" => $value->ansA,
                                        "notes" => $value->notes1,
                                        "value" => "A"
                                    ],
                                    [
                                        "name" => $value->ansB,
                                        "notes" => $value->notes2,
                                        "value" => "B"
                                    ],
                                    [
                                        "name" => $value->ansC,
                                        "notes" => $value->notes3,
                                        "value" => "C"
                                    ],
                                    [
                                        "name" => $value->ansD,
                                        "notes" => $value->notes4,
                                        "value" => "D"
                                    ]
                                ]
                            ];
                        })->values()
                    ]
                ];
            })->values();
        }


        // dd($questionnum2);

        $read = collect();
        if (isset($data["5"])) {
            $read = $data["5"]->map(
                function ($value, $index) use (&$questionnum, &$questionAns, &$TopQuestionNum) {
                    $TopQuestionNum++;
                    return [
                        "read" => [
                            "art" => $value[0]->art,
                            "art_notes" => $value[0]->art_notes,
                            "data" => $value->map(function ($value, $keys) use (&$questionnum, &$questionAns, &$TopQuestionNum) {
                                $questionnum++;
                                $questionAns->push([
                                    "ans" => [$value->correct_ans1],
                                    "group" => $TopQuestionNum
                                ]);
                                return [
                                    "questionnumber" => $questionnum,
                                    "status" => false,
                                    "selectq" => "",
                                    "ans" =>[$value->correct_ans1],
                                    // "art" => strip_tags($value->art),
                                    "qa_ans_id" => $value->qa_ans_id,
                                    "grp" => $value->grp,
                                    "title" => $value->questions_cname,
                                    "notes_title" => $value->notes,
                                    "sel" => [
                                        [
                                            "name" => $value->ansA,
                                            "notes" => $value->notes1,
                                            "value" => "A"
                                        ],
                                        [
                                            "name" => $value->ansB,
                                            "notes" => $value->notes2,
                                            "value" => "B"
                                        ],
                                        [
                                            "name" => $value->ansC,
                                            "notes" => $value->notes3,
                                            "value" => "C"
                                        ],
                                        [
                                            "name" => $value->ansD,
                                            "notes" => $value->notes4,
                                            "value" => "D"
                                        ]
                                    ]
                                ];
                            })->values()
                        ]
                    ];
                }
            )->values();
        }

        // for ($i = 0; $i < $fillin->count(); $i++) {
        //     $single->push($fillin[$i]);
        // }

        // for ($i = 0; $i < $read->count(); $i++) {
        //     $single->push($read[$i]);
        // }

        $aaa = $single->concat($plural)->concat($fillin)->concat($read);
        return [
            "status" => true,
            "method" => $method,
            "totalQuestion" => $questionnum,
            "questionAns" => $questionAns,
            "result" => $aaa,

        ];
    }
    public function getQuestions(Request $request)
    {
        // $type = collect([["name" => "CG2", "tabel" => "QUESTIONS"]]);
        // // exam_class;
        // $selectType = $type->filter(function ($item, $key) use ($request) {
        //     return $item["name"] == $request["exam_type"];
        // });


        // return $selectType;
        // $obj = new SwitchTables;
        // return $obj->switch($request->selectTables);
        return $this->getexampage($request->type, $request->year, $request->examId, $request->method);



        // if ($request->type == "C") {
        //     return $this->questions($year, $request->examId);
        // } else if ($request->type == "E") {
        //     return $this->eng($year, $request->examId);
        // } else if ($request->type == "J") {
        //     return $this->jap($year, $request->examId);
        // }
        // if ($request->selectTables == 0) {
        //     return $this->questions($request->qaKind, $year, "UL3");
        // } else if ($request->selectTables == 1) {
        //     return $this->jap($request->qaKind, $year, "UL3");
        // } else if ($request->selectTables == 2) {
        //     return $this->eng($request->qaKind, $year, "UL3");
        // }
        //return SwitchTables::switch($request->selectTables);
    }
    public function jap($year, $examId)
    {
        //每一個參數 以直接能應用為主
        // jap ($a ,$b,$c)
        //4*jap 3 變數直接去判別 where 1=>$a 2 =>$b 3 => $c /1 where 2  1=>$a 2-> $b+$c
        return JAP_DATA::where("exam_id", '=', "EXAM_$year$examId")->get();
    }
    public function eng($year, $examId)
    {
        /*where("qa_kind_id", '=', $qaKind)
            ->where("year", '=', $year)
            ->
        */
        return ENG_DATA::where("exam_id", '=', "EXAM_$year$examId")->get();
    }
    public function questions($year, $examId)
    {
        //return "EXAM_$year$qaKind2";
        return QUESTIONS::where("exam_id", '=', "EXAM_$year$examId")->get();
    }
}
