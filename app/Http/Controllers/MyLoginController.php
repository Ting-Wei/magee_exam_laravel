<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Record;
use App\record_new;
use App\member_new;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class MyLoginController extends Controller
{
    public function login(Request $request)
    {
        $user1 = Auth::guard('api')->user();
        $record = new record_new();
        //$user = Auth::attempt('api');
        // dd($user);
        if ($user1) {
            $questionsRecord = $record->where("mem_id", "=", $user1->id)->get();
            $Authority = false;
            if ($user1->member_level == "admin") {
                $Authority = true;
            } else {
                $Authority = false;
            }
            return [
                "status" => true,
                "Authority" => $Authority,
                'id' => $user1->id,
                'member_id' => $user1->member_id,
                'api_token' => $user1->api_token,
                "questions_record" => $questionsRecord,
            ];
        } else {
            $pwd = member_new::where('member_id', $request->member_id)->pluck('password')->first();
            // $check = Hash::check($request->password, $pwd);
            $check = false;
            if ($pwd !== null) {
                if ($request->password == $pwd) {
                    $check = true;
                } else {
                    $check = false;
                }
            } else {
                $check = false;
            }
            $api_token = Str::random(60);


            if ($check == true) {
                $user = member_new::where('member_id', $request->member_id)->update(['api_token' => $api_token]);
                $userInfo = member_new::where('member_id', $request->member_id)->first();
                $questionsRecord = $record->where("mem_id", "=", $userInfo->id)->get();
                $Authority = false;
                if ($userInfo->member_level == "admin") {
                    $Authority = true;
                } else {
                    $Authority = false;
                }
                return [
                    "status" => true,
                    "Authority" => $Authority,
                    'id' => $userInfo->id,
                    'member_id' => $userInfo->member_id,
                    'api_token' => $userInfo->api_token,
                    "questions_record" => $questionsRecord,
                ];
            } else {
                return [
                    "status" => false,
                ];
            }
        }
        // dd($request->headers);
        // return;
    }
    public function GetRecord(Request $request)
    {
        $record = new record_new();
        return $record->where("mem_id", "=", $request->id)->get();
    }
    public function BuyQuestion(Request $request)
    {
        // return $request;
        $record = new record_new();
        $record->mem_id = $request->id;
        $record->exam_re = $request->examId;
        $record->save();
        return $record->where("mem_id", "=", $request->id)->get();
    }
}
