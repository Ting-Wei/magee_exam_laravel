<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Member extends Authenticatable
{
    use Notifiable;
    protected $connection = 'mysql2';
    protected $table = 'MEMBER';
    protected $fillable = [
        'member_id', 'password', 'api_token'
    ];
}
