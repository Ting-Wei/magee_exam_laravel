<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QUESTIONS extends Model

{
    protected $connection = 'mysql2';
    protected $primaryKey = 'id';
    protected $table = 'QUESTIONS';
}
