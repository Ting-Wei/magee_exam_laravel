<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JAP_DATA extends Model
{
    protected $connection = 'mysql2';
    protected $primaryKey = 'id';
    protected $table = 'JAP_DATA';
}
