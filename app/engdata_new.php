<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class engdata_new extends Model
{
    public $timestamps = false;
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'eng_data';
    protected $guarded = [];
}
