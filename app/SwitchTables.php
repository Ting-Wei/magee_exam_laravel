<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\QUESTIONS;
use App\ENG_DATA;
use App\JAP_DATA;

class SwitchTables
{
    public static function switch($id)
    {
        if ($id == 0) {
            $questions = QUESTIONS::all();
            return $questions->skip(0)->take(1);
        } else if ($id == 1) {
            $jap_data = JAP_DATA::all();
            return $jap_data->skip(0)->take(1);
        } else if ($id == 2) {
            $eng_data = ENG_DATA::all();
            return $eng_data->skip(0)->take(1);
        }
    }
}
