<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateEngDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eng_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uid', 256);
            $table->text('orders', 125);
            $table->text('remark', 256);
            $table->timestamp('createtime')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('cmember');
            $table->integer('umember');
            $table->string('sys_company', 125);
            $table->text('nydel');
            $table->integer('year');
            $table->string('qa_kind_id', 256);
            $table->string('qa_ans_id', 256);
            //$table->string('qa_lan_id', 256);
            //$table->string('course_id', 256);
            $table->string('grp', 256);
            $table->text('art');
            $table->text('questions_cname');
            $table->text('ansA');
            $table->text('ansB');
            $table->text('ansC');
            $table->text('ansD');
            $table->text('notes');
            $table->text('notes1');
            $table->text('notes2');
            $table->text('notes3');
            $table->text('notes4');
            $table->text('art_notes');
            $table->text('pic');
            $table->text('art_tip');
            $table->string('correct_ans1', 256);
            $table->string('correct_ans2', 256);
            $table->string('correct_ans3', 256);
            $table->string('correct_ans4', 256);
            $table->string('idept_id', 256);
            $table->string('isdept_id', 256);
            $table->string('iclas_id', 256);
            $table->string('isclas_id', 256);
            $table->string('editor_type_id', 256);
            $table->string('exam_id', 256);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eng_data');
    }
}
