<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uid', 256);
            $table->text('orders', 125);
            $table->text('remark', 256);
            $table->timestamp('createtime')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('cmember');
            $table->integer('umember');
            $table->string('sys_company', 125);
            $table->text('nydel');
            $table->string('member_id', 256);
            $table->string('password', 256);
            $table->string('member_cname', 256);
            $table->string('id_no', 256);
            $table->string('gender_id', 256);
            $table->date('birth_date');
            $table->string('post_id', 256);
            $table->string('address');
            $table->string('member_type_id', 256);
            $table->string('phone_no', 256);
            $table->string('mobile_no', 256);
            $table->string('email', 256);
            $table->string('epaper', 256);
            $table->string('img', 256);
            $table->string('menver_img', 256);
            $table->string('point', 256);
            $table->string('member_level', 256);
            $table->string('member_jobtype', 256);
            $table->string('member_edu', 256);
            $table->string('member_worksatae', 256);
            $table->string('member_department', 256);
            $table->string('member_license', 256);
            $table->string('member_skill', 256);
            $table->string('pipeline', 256);
            $table->string('api_token', 256);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member');
    }
}
