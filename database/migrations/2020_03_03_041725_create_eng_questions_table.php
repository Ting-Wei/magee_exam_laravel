<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateEngQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eng_questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uid', 256);
            $table->text('orders', 125);
            $table->text('remark', 256);
            $table->timestamp('createtime')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('cmember');
            $table->integer('umember');
            $table->string('sys_company', 125);
            $table->text('nydel');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eng_questions');
    }
}
