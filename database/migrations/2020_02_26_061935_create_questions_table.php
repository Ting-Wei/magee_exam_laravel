<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uid', 256)->default('');
            $table->text('orders', 125)->default(null);
            $table->text('remark', 256)->default(null);
            $table->timestamp('createtime')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('cmember')->default(0);
            $table->integer('umember')->default(0);
            $table->string('sys_company', 125)->default('');
            $table->text('nydel')->default(null);
            $table->integer('year')->default(null);
            $table->text('questions_cname');
            $table->string('qa_kind_id', 256)->default('');
            $table->string('qa_ans_id', 256)->default('');
            $table->string('qa_lan_id', 256)->default('');
            $table->string('course_id',256)->default('');
            $table->string('ansA', 256)->default('');
            $table->string('ansB', 256)->default('');
            $table->string('ansC', 256)->default('');
            $table->string('ansD', 256)->default('');
            $table->string('notes', 256)->default('');
            $table->string('notes1', 256)->default('');
            $table->string('notes2', 256)->default('');
            $table->string('notes3', 256)->default('');
            $table->string('notes4', 256)->default('');
            $table->string('correct_ans1', 256)->default('');
            $table->string('correct_ans2', 256)->default('');
            $table->string('correct_ans3', 256)->default('');
            $table->string('correct_ans4', 256)->default('');
            $table->string('idept_id', 256)->default('');
            $table->string('isdept_id', 256)->default('');
            $table->string('iclas_id', 256)->default('');
            $table->string('isclas_id', 256)->default('');
            $table->string('idept_cname', 256)->default('');
            $table->string('isdept_cname', 256)->default('');
            $table->string('iclas_cname', 256)->default('');
            $table->string('isclast_cname', 256)->default('');
            $table->string('editor_type_id', 256)->default('');
            $table->string('exam_id', 256)->default('');


            // $table->string('brows_order', 10);

            // $table->string('online', 100);
            // $table->string('online_order', 10);
            // $table->date('online_date');
            // $table->date('offline_date');
            // $table->string('online_name', 100);

            // $table->string('crea_user', 100);
            // // $table->timestamp('upd_date');
            // $table->string('upd_user', 100);
            // $table->string('login_ip', 100);
            // $table->string('com_id', 100);
            // $table->string('uid', 100);
            // $table->string('data_sts', 10);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
